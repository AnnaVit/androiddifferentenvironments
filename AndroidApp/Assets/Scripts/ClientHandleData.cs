﻿using Assets.Externals;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Assets.Scripts.DataReceiver;

namespace Assets.Scripts
{
    class ClientHandleData
    {
        private static Dictionary<int, Packet> packets = new Dictionary<int, Packet>();
        private static ByteBuffer playerBuffer;

        public delegate void Packet(byte[] data);

        public static void InitializePackets()
        {
            packets.Add((int)ServerPackets.SWelcomeMessage, DataReceiver.HandleWelcomeMsg);
            packets.Add((int)ServerPackets.SRemoveControls, DataReceiver.HandleRemoveControls);
            packets.Add((int)ServerPackets.SRestoreControls, DataReceiver.HandleRestoreControls);
            packets.Add((int)ServerPackets.SUpdatePosition, DataReceiver.HandleUpdatePosition);
            packets.Add((int)ServerPackets.SUpdateRotation, DataReceiver.HandleUpdateRotation);
            packets.Add((int)ServerPackets.SUpdateScale, DataReceiver.HandleUpdateScale);
        }

        //prevents package lost
        public static void HandleData(byte[] data)
        {
            byte[] buffer = (byte[])data.Clone();
            int pLength = 0;

            if (playerBuffer == null)
            {
                playerBuffer = new ByteBuffer();
            }

            playerBuffer.WriteBytes(buffer);

            if (playerBuffer.Count() == 0)
            {
                playerBuffer.Clear();
                return;
            }

            if (playerBuffer.Lenght() >= 4)
            {
                pLength = playerBuffer.ReadInt(false);

                if (pLength <= 0)
                {
                    playerBuffer.Clear();
                    return;
                }

            }

            while (pLength > 0 & pLength <= playerBuffer.Lenght() - 4) //prima leggiamo l'int della lunghezza
            {
                if (pLength <= playerBuffer.Lenght() - 4)
                {
                    playerBuffer.ReadInt();
                    data = playerBuffer.ReadBytes(pLength);
                    HandleDataPackets(data);
                }

                pLength = 0;

                if (playerBuffer.Lenght() >= 4)
                {
                    pLength = playerBuffer.ReadInt(false);

                    if (pLength <= 0)
                    {
                        playerBuffer.Clear();
                        return;
                    }
                }
            }

            if (pLength <= 1)
            {
                playerBuffer.Clear();
            }
        }

        private static void HandleDataPackets(byte[] data)
        {
            ByteBuffer buffer = new ByteBuffer();
            buffer.WriteBytes(data);

            int packetID = buffer.ReadInt();
            buffer.Dispose();

            if (packets.TryGetValue(packetID, out Packet packet))
            {
                packet.Invoke(data);
            }

        }
    }
}
