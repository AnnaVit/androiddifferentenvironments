﻿using Assets.Scripts;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ReadInput : MonoBehaviour
{
    private string input;

    [SerializeField]
    GameObject textInputField;
    [SerializeField]
    GameObject debugLog;

    [SerializeField]
    GameObject inputField;
    [SerializeField]
    GameObject button;

    [SerializeField]
    NetworkManager networkManager;

    public void Readtext()
    {
        this.input = textInputField.GetComponent<Text>().text;
        IPAddress address;
        if (IPAddress.TryParse(input, out address))
        {
            inputField.SetActive(false);
            button.SetActive(false);

            debugLog.GetComponent<Text>().text = "Trying to connect to the server...";

            networkManager.ConnectClient(this.input);
        }
        else {
            debugLog.GetComponent<Text>().text = "This isn't a valid IP address";
        }

        

    }
}
