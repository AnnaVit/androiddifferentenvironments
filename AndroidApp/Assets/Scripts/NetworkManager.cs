﻿using Assets.Scripts;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NetworkManager : MonoBehaviour
{

    private static NetworkManager instance;

    private void Awake()
    {
        instance = this;
    }

    // Start is called before the first frame update
    void Start()
    {
        DontDestroyOnLoad(this);
        UnityThread.initUnityThread();

        ClientHandleData.InitializePackets();
    }

    private void OnApplicationQuit()
    {
        ClientTCP.Disconnect();
    }

    public void ConnectClient(string address)
    {
        ClientTCP.ServerAddress = address;
        ClientTCP.InitializingNetworking();
    }
}
