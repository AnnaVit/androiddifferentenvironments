﻿using Assets.Scripts;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum Manipulation { Mooving = 1, Rotating = 2, Scaling = 3, None = 4 }

public class UpdateObject : MonoBehaviour
{
    [SerializeField]
    GameObject hologram;

   [SerializeField]
    GameObject debugLog;

    private float timeUpdate = 0.30f;
    private Manipulation currentState;
    private Color myColor;
   
    public Manipulation State { get => currentState; set => currentState = value; }
    public Color MyColor { get => myColor; set => myColor = value; }

    public void ShowHologram()
    {
        hologram.SetActive(true);
        debugLog.SetActive(false);
    }

    public void RequestControl(int state)
    {
        this.currentState = (Manipulation)state;
        DataSender.RequestControl(JsonUtility.ToJson(myColor));
    }

    public void ReleseControl()
    {
        this.currentState = Manipulation.None;
        DataSender.ReleseControl();
    }

    private void Start()
    {
        DataReceiver.UpdateObject = this;
    }

    private void Update()
    {
        switch (currentState)
        {
            case Manipulation.Mooving:
                this.SendPosition();
                this.DelayTime(timeUpdate);
                break;
            case Manipulation.Rotating:
                this.SendRotation();
                this.DelayTime(timeUpdate);
                break;
            case Manipulation.Scaling:
                this.SendScale();
                this.DelayTime(timeUpdate);
                break;
            case Manipulation.None:
         
                break;
        }

  
    }

    private IEnumerable DelayTime(float time)
    {
        yield return new WaitForSeconds(time);
    }

    private void SendRotation()
    {
        DataSender.SendRotation(JsonUtility.ToJson(hologram.transform.rotation));
        //debugLog.GetComponent<Text>().text = "send rotation";
    }

    private void SendScale()
    {
        DataSender.SendScale(JsonUtility.ToJson(hologram.transform.localScale));
        //debugLog.GetComponent<Text>().text = "send scale";
    }

    private void SendPosition()
    {
        DataSender.SendPosition(JsonUtility.ToJson(hologram.transform.position));
        //debugLog.GetComponent<Text>().text = "send position";
    }

    public void UpdatePosition(Vector3 position)
    {
        hologram.transform.position = position;
    }

    public void UpdateRotation(Quaternion rotation)
    {
        hologram.transform.rotation = rotation;
    }

    public void UpdateScale(Vector3 scale)
    {
        hologram.transform.localScale = scale;
    }
}
