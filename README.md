# README #

Questo repository rappresenta il programma da poter installare ed eseguire su un dispositivo Android, il quale consente di poter partecipare a un'esperienza condivisa stando in ambienti diversi, potendo vedere le manipolazioni effettuate dagli altri utenti, che possono accedere tramite un dispositivo Android o un dipositivo HoloLens 2.

### Premessa utilizzo della besh ###

Per poter utilizzare questo repository sul vostro PC, si è notato che l'utilizzo di bash come *GIT Bash* o *Windows Powershell* per salvare i file modificati nel repository, genera dei problemi a causa dei packages dell'MRTK che sono molti e presentano nomi troppo lunghi per queste besh, per questo motivo si è proceduto all'installazione e all'utilizzo di una *Linux besh*, che invece non ha dato questi porblemi ed è risultata essere anche abbastanza veloce nel salvataggio delle modifiche, se possedete un PC Windows potete trovare le istruzioni su come ottenere la bash al seguente link: https://docs.microsoft.com/it-it/windows/wsl/install-win10 oppure se siete in difficoltà, potete seguire questo tutorial https://www.youtube.com/watch?v=5W5EahK9ubo&t=412s.

### Istruzioni per l'esecuzione ###

 - Una volta importato il repository, aggiungere il progetto ad Unity Hub e procedere alla sua apertura.
 - Una volta che il porgetto è stato aperto correttamente, spostarsi nella scena principale, andando su **Assets/Scenes/** aprendo la scena *MainScene.unity*.
 - Aprire le impostazioni di progetto cliccando su **File** -> **Build Settings** a questo punto cambiare:

   - La piattaforma in *Android* cliccando sul pulsante **Switch Platform** dopo la selezione,
   - In Run Device selezionate il vostro dispositivo, il quale sarà visibile una volta collegato al PC.

- Dalle impostazioni di Build cliccare sul pulsante **Player Settings**, andare sulla voce **Player** e verificare che:

  - La piattaforma scelta sia *Android*, quindi che l'icona selezionata sia quella di Android,
  - Aprire il menu **Other Settings** e verificare che nelle **Graphics APIs** non sia presente l'API *Vulkan*
  - Sempre in **Other Settings** verificare che il Minimum API Level sia impostato su *24*


- Spostarsi nella scena principale e cliccare l'elemento **MixedRealityToolkit**,
- Verificare che il profilo scelto sia *AndroidAppMixedRealityToolkitConfigurationProfile*.

### Istruzioni per il deployment ###

- Andando in **File** -> **Build Settings** si può procedere alla build del porgetto cliccando sul pulsante **Build and Run**, creando una nuova cartella **Build** all'interno del progetto dove poter salvare l'apk, ricordando che questa cartella non verra tracciata da git,
- Al termine della build, se tutto è avvenuto correttamente l'applicazione dovrebbe aprirsi automaticamente sul vostro dispositivo
- Si potrà inoltre trovare l'applicazione sul dispositivo con il nome *AndroidApp*, che può essere modificato spostandosi in Unity andando su **Build Settings** -> **Player Settings** alla voce **Player** selezionando il menu **Icon**, cercando la voce **Short Name**.

### Problema "Black screen" ###

- Se all'avvio dell'applicazione la fotocamera non si apre corretamente ma vi viene mostrato uno schermo nero vi sono due modi in cui poterlo risolvere:
  1. Richiudete l'applicazione sul dispositivo Android e riapritela aspettando qualche secondo prima di toccare l'interfaccia grafica
  2. Se il primo metodo non ha prodotto i risultati attesi, ritornate su Unity, nel menù in alto cliccate su **Mixed Reality Toolkit** -> **Utilities** -> **UnityAR** -> **Update Scripting Define**.

### Note da Ricordare ###

L'indirizzo del server viene immesso all'avvio dell'applicazione dall'utente, tramite l'apposita GUI che li viene presentata, se si sta utilizzando l'indirizzo IP privato del server allora, **client e server per poter comunicare devono essere connessi alla stessa rete**, verificare questo tramite le impostazioni di rete.

Una volta che la connessione è andata a buon fine si potrà visualizzare l'ologramma e procedere alla sua manipolazione.
